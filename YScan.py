#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time
from utils.crt import Crt
from utils.yumingco import Yumingco
from utils.ip138 import Ip138
from utils.hackertarget import Hackertarget
from utils.virusTotal import Virus
from utils.cesuyun import Cesuyun
from utils.brutedomain import Brutedomain
from utils.beian import Beian
from utils.GetIp import GetIp
from utils.cms import Cms
from utils.iscdn import iscdn1
from utils.JS import JS
from utils.Fofa import Fofa
#from utils.Cduan import Cduan
from utils.Whois import Whois
from utils.leakinfo import leakinfo
from utils.Checkwaf import Checkwaf
from common import *
import sys

def main():
    print('当前域名'+get_domain_root(url))
    # # ============IP信息============== # #
    result_ip = GetIp(domain = GetURL(url)).run()
    # # ============备案查询============== # #
    result_whois = Whois(domain = get_domain_root(url)).run()
    # # ============备案查询============== # #
    result_beian = Beian(domain=get_domain_root(url)).run()  #备案查询
    # # ============CMS查询============== # #
    result_cms = Cms(domain = GetURL(url)).run()
    # # ============判断CDN============== # #
    result_cdn = iscdn1(domain=GetURL(url)).run()
    # # ============WAF检测============== # #
    result_Checkwaf = Checkwaf(domain=url).run()
    # # ============爬取JS============== # #
    result_js_url,result_js_subdomain = JS(domain=url).run()
    # # ============相同ICON网站============== # #
    result_fofa = Fofa(domain=GetURL(url)).run()
    # # ============敏感信息监测============== # #
    result_leakinfo = leakinfo(domain=url).run()
    # # ============子域名收集============== # #
    result1 = Crt(domain = get_domain_root(url)).run()
    result2 = Yumingco(domain = get_domain_root(url)).run()
    result3 = Ip138(domain = get_domain_root(url)).run()
    result4 = Hackertarget(domain=get_domain_root(url)).run()
    result5 = Virus(domain = get_domain_root(url)).run()
    result6 = Cesuyun(domain = get_domain_root(url)).run()
    # 将所有列表合并，方便去重整理结果
    result_end(result1)   #crt查询结果
    result_end(result2)   #yumingco查询结果
    result_end(result3)   # ip138查询结果
    result_end(result4)   #hackertarget查询结果
    result_end(result5)   # virustotal查询
    result_end(result6)   #Cesuyun查询
    result_end(result_js_subdomain)
    result_chaxun = list(set(resultall))    #去重后的结果
    #result_chaxun1 = list(set(resultall1))
    print("[*]整合结果中(去重)[*]")
    print_try("[*]整合完成，共"+str(len(result_chaxun))+"个域名[*]")
    #time.sleep(2)

    print("查询结果：\n")
    print("\n======IP======")
    print(result_ip)
    print("\n======Whois======")
    print(result_whois)
    print("\n======备案信息======")
    print(result_beian)
    print("\n======CMS查询======")
    print(result_cms)
    print("\n======是否存在CDN======")
    print(result_cdn)
    print("\n======是否存在WAF======")
    print(result_Checkwaf)
    print("\n======相同ICON_HASH网站======")
    print(result_fofa)
    print("\n======敏感信息======")
    for z in result_leakinfo:print(z)
    print("\n======JS相关链接======")
    if result_js_url:
        for y in result_js_url:
            print(y)
    else:
        print("None")
    print("\n======子域名======")
    print_result(result_chaxun)
    return result_chaxun

def brute_main():

    # ============子域名爆破============== #

    result_baopo = Brutedomain(domain = get_domain_root(url),thread_count=args.thread).run()
    # 将所有列表合并，方便去重整理结果
    result_end(result_baopo)    #爆破结果
    result_all = list(set(resultall))    #去重后的结果
    print_result(result_all)
    Baocun(url,result_all)      #将最终的结果保存到**.**.**.txt文件中


if __name__ == '__main__':
    print('''
__   ______   ____    _    _   _ 
\ \ / / ___| / ___|  / \  | \ | |
 \ V /\___ \| |     / _ \ |  \| |
  | |  ___) | |___ / ___ \| |\  |
  |_| |____/ \____/_/   \_\_| \_|
                               By:Exp1ore
                
        ''')
    print("Use: python3 Yscan.py domain Thread")
    print("Example: python3 Yscan.py -u baidu.com -t 100")
    print("Example: python3 Yscan.py -f url.txt -t 100")
    args = parse_args()
    if args.file == None:
        url = Url(args.url)
        main()
        user_input3(url)
        user_input(url,list(set(resultall)))
        brute_main()
        exit()
    else:
        for i in open(args.file,'r'):
            url = i.replace('\n','')
            main()
            Baocun(url,list(set(resultall)))
            resultall.clear()       # 清空列表，方便保存下一个url域名
        user_input4()
        user_input2()
        for j in open(args.file,'r'):
            url = j.replace('\n', '')
            brute_main()
            resultall.clear()
    End()
    exit()
