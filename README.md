# Yscan

#### 介绍

在红蓝对抗中，信息收集是必不可少的环节，这么才可以从大量的资产中提取有用的系统，这个工具适用于以HW行动/红队/渗透测试团队为场景的信息收集扫描工具。可以帮助渗透测试工程师、攻击队成员、红队成员快速收集到资产信息并提供基本的信息输出


#### 功能介绍:

 :fa-check-square-o: 支持获取IP  
 :fa-check-square-o: 支持Whois查询  
 :fa-check-square-o: 支持备案信息查询  
 :fa-check-square-o: 支持CMS指纹识别  
 :fa-check-square-o: 支持获取相同ICON网站  
 :fa-check-square-o: 支持CDN判断  
 :fa-check-square-o: 支持C段查询及端口检测  
 :fa-check-square-o: 支持敏感信息探测  
 :fa-check-square-o: 支持子域名查询  
 :fa-check-square-o: 支持WAF检测  
 :fa-check-square-o: 支持爬取JavaScript提取URL及子域名  
 :fa-square-o: 支持各端口系统服务识别    
 :fa-square-o: 支持目录扫描  
 :fa-square-o: 支持相关网站Github信息泄露收集  
 :fa-square-o: 支持收集域名邮箱及判断存活  
 :fa-square-o: 支持丰富的输出报告（txt\html\pdf\word）  
 :fa-square-o: 支持可配置检测模块 (比如只检测子域名信息、敏感信息等)  
 :fa-square-o: 未完待续 

#### 程序截图

![输入图片说明](https://images.gitee.com/uploads/images/2021/0624/004222_4a0cf296_4992716.png "屏幕截图.png")

#### 环境说明

Python3运行环境


#### 程序结构


```
.  
├── YScan.py  #主程序  
├── common.py    
├── config.py  #配置信息  
├── dict  
│   ├── GeoLite2-ASN.mmdb  
│   ├── dict.txt  
│   ├── rules.txt  
├── requirements.txt  
├── result  #结果保存  
│   └── test.txt  
└── utils  
    ├── Cduan.py  #C段查询  
    ├── Fofa.py  #ICON_HASH查询
    ├── Whois.py  #Whois查询  
    ├── GetIp.py  #获取IP  
    ├── JS.py  #爬取JS提取URL子域名  
    ├── beian.py  #ICP查询  
    ├── leakinfo.py #敏感信息探测  
    ├── Checkwaf.py  #waf检测
    ├── brutedomain.py  #子域名爆破  
    ├── cesuyun.py  #子域名接口1  
    ├── cms.py  #CMS识别  
    ├── crt.py  #子域名接口2  
    ├── hackertarget.py  #子域名接口3  
    ├── ip138.py  #子域名接口4  
    ├── iscdn.py  #判断CDN  
    ├── virusTotal.py  #子域名接口5  
    └── yumingco.py  #子域名接口6  
```


#### 使用说明

1. 下载  

```
git clone https://gitee.com/exp1ore/yscan.git
```

2. 安装依赖库  

```
cd yscan
pip install -r requirements.txt
```

3.配置  
 #1 如果要进行C段扫描  
 ①需安装Goby并打开  
 下载地址：https://gobies.org/#dl  
![输入图片说明](https://images.gitee.com/uploads/images/2021/0619/013748_c31c0c38_4992716.png "屏幕截图.png")
 ②安装goby模块  
   
```
git clone https://github.com/Becivells/goby-api.git
cd goby-api
python setup.py install
```

 #2 如果要进行ICON_HASH查询
 需配置fofa api且为fofa高级会员  
https://gitee.com/exp1ore/yscan/blob/master/utils/Fofa.py(#15 #16)

为方便演示，暂时可以使用

4.运行

对单目标进行信息收集
```
python3 Yscan.py -u baidu.com -t 100
```
对多目标进行信息收集
```
python3 Yscan.py -f url.txt -t 100
```


#### 参考

https://github.com/shmilylty/OneForAll

https://github.com/Threezh1/JSFinder

https://github.com/al0ne/Vxscan

https://github.com/Becivells/goby-api

https://websec.readthedocs.io/zh/latest/index.html

https://github.com/dyboy2017/TScan


