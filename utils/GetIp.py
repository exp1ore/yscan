#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 通过socket解析网站ip
import re
from common import http_requests_get, is_domain, print_try
import socket


class GetIp(object):      #定义一个类
	def __init__(self,domain):     #初始化
		self.domain = domain    # 接受域名信息
		self.result = []    # 定义一个列表，接受返回结果
	def run(self):
		print("[*]正在通过域名反查ip[*]")
		#print(self.domain)
		try:
			myaddr = socket.getaddrinfo(self.domain,None)[0][4][0]
			print("成功获取IP")
			return myaddr
		except Exception as e:
			print("获取失败")
			return (e)	

