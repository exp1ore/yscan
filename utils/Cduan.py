#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 通过Goby扫描网站C段
from goby.v1 import GobyApi
from goby.v1 import data
import time 
import socket

class Cduan(object):      #定义一个类
	def __init__(self,domain):     #初始化
		self.domain = domain    # 接受域名信息
	def gp(self,domain):
		try:
			myaddr = socket.getaddrinfo(self.domain,None)[0][4][0]
			return myaddr
		except Exception as e:
			return None
	def run(self):
		z = GobyApi()
		#print(z.client.version())
		envi = z.client.getEnvi()
		#print(envi)

		target = self.gp(self.domain)
		#print(target)
		if target != None:
			scanresult = z.client.startScan(ips=[str(target)+"/24"], ports=data.ports.ALL_Ports, opt_interface="en0")
			#print(scanresult)


			taskId = (scanresult['data']['taskId'])
			#taskId="20210615173612"
			a = []
			while True:
				#print(taskId)
				progress = z.client.getProgress(taskId)["data"]["progress"]
				print("C段扫描进度:"+str(progress)+"/100")
				time.sleep(10)
				if progress == 100:
					print("扫描完成!")
					re = z.asset.getHostLists(taskId)['data']['lists'].keys()
					a = []

					for i in re:
						s = z.asset.getIPInfo(taskId,i)['data']
						if "ports" in s.keys():
							b = z.asset.getIPInfo(taskId,i)['data']['ports']
							for x in range(len(b)):
								a.append(i+":"+b[x]['port'])
							#for j in z.asset.getIPInfo(taskId,i)['data']['ports']['port']:
								#print(str(i)+str(j))
						else:
							a.append("该ip"+i+"没有存活端口")
						#if z.asset.getIPInfo(taskId,i)['data']['ports'] != None:
							#a.append(z.asset.getIPInfo(taskId,i)[i]['data']['ports'])
						#else:
							#a.append("None")
					#print(a)
					#print(re)
					break
			return a
		else:
			print("未检测出源ip")
            