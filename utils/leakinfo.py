#!/usr/bin/env python
# -*- coding: utf-8 -*-
#通过匹配指纹探测敏感信息文件

import requests
from config import *
import re,os,sys,time


from requests.packages.urllib3.exceptions import InsecureRequestWarning,InsecurePlatformWarning   # 屏蔽错误提示的一般方法，配合下面两个disable
import requests.packages.urllib3.util.ssl_                   # 解决部分ssl证书版本不正确的问题
requests.packages.urllib3.util.ssl_.DEFAULT_CIPHERS = 'ALL'
requests.packages.urllib3.disable_warnings(InsecureRequestWarning) # 移除ssl错误告警
requests.packages.urllib3.disable_warnings(InsecurePlatformWarning)

class leakinfo(object):
    def __init__(self,domain):
        self.domain = domain
        self.result =[]
    def run(self):
        print("[*]正在进行敏感信息检测[*]")
        f = open('dict/rules.txt','r',encoding='utf-8')
        txt = f.readlines()
        #res = []
        for x in txt:
            u,j,w = x.strip().split('|')
            url = self.domain + u
            #print(url)
            try:
                r = requests.get(url,headers=headers,timeout=3,verify=False) # http
                html = r.text
                if r.status_code == 200:
                    if j in html:
                        self.result.append(url+' 存在 '+ w)
                    else:
                        self.result.append(url+' 存在 !!!')
            except Exception as e:
                pass
        print('检测完成')
        return(self.result)
    