#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Whois查询

import whois

class Whois(object):
    def __init__(self,domain):
        self.domain = domain
    def run(self):
        print("[*]正在进行Whois查询[*]")
        try:
            w = whois.whois(self.domain)
            r = dict(w)
            res = "域名:" + r["domain_name"] + "\n" + "注册商:" + r["registrar"] + "\n" + "域名持有人/机构名称:" + r["name"] + "\n" + "域名持有人/机构邮箱:" + r["emails"] +"\n" + "创建时间:" + str(r["creation_date"]) + "\n" + "过期时间:" + str(r["expiration_date"]) + "\n" + "DNS服务器:" + (",".join(r["name_servers"])) + "\n" 
            print('Whois查询完成')
            return res
        except Exception as e:
            return e
