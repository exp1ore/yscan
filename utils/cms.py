#!/usr/bin/env python
# -*- coding: utf-8 -*-
# 指纹识别

from config import *
#from common import http_requests_getJSON
import requests

class Cms(object):
    def __init__(self,domain):
        self.domain = domain
        self.site = 'https://whatcms.org/APIEndpoint/Detect'
        self.payload = {'key': '1641c3b9f2b1c8676ceaba95d00f7cf2e3531830c5fa9a6cc5e2d922b2ed7165dcce66', 'url': self.domain}
        self.result = []
    def run(self):
        print("[*]正在进行指纹识别[*]")
        try:
            response = requests.get(url = self.site, params=self.payload)
            data = response.json()
            info = data['result']
            #print(info)
            if info['code'] == 200:
                res=("Detected CMS:"+info['name']+"\n"+"Detected Version :" + info['version']+"\n"+"Confidence:" +info['confidence'])
            else:
                res='未识别出CMS'
            print('指纹识别完成')
            return res
        except Exception as e:
            return e
